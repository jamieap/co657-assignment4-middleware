package uk.ac.kent.jb695.co657.java.app.messaging.messages;

import com.google.common.base.Enums;
import com.google.common.base.Optional;
import uk.ac.kent.jb695.co657.java.app.messaging.topics.MessageTopic;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Attempts to turn a {@link String} into a {@link Message} by matching it to a
 * regular expression and checking its value can be associated with a {@link MessageTopic}
*/
public class MessageParser {

    /* 0909 is an identifier to 'uniquely' ID the expected device, it also acts as some
        excuse for a password */
    private static final Pattern MESSAGE_PARSER_PATTERN = Pattern.compile(
            "0909([A-Z]+) ([0-9]+\\.[0-9]{2})."
    );

    public Optional<Message> parseMessage(String message) {
        Matcher matcher = MESSAGE_PARSER_PATTERN.matcher(message);
        if (!matcher.matches()) {
            return Optional.absent();
        }
        Optional<MessageTopic> maybeTopic = Enums.getIfPresent(MessageTopic.class, matcher.group(1));
        if (!maybeTopic.isPresent()) {
            return Optional.absent();
        }
        return Optional.of(new Message(matcher.group(2), maybeTopic.get()));
    }
}