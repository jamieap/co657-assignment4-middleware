package uk.ac.kent.jb695.co657.java.app.messaging.handlers;

import org.jfree.data.time.Second;
import uk.ac.kent.jb695.co657.java.app.Utils;
import uk.ac.kent.jb695.co657.java.app.gui.HourlyHighLowTemperatureChart;
import uk.ac.kent.jb695.co657.java.app.gui.RawHistoryTemperatureChart;
import uk.ac.kent.jb695.co657.java.app.gui.TemperatureChart;
import uk.ac.kent.jb695.co657.java.app.messaging.messages.Message;
import uk.ac.kent.jb695.co657.java.app.messaging.topics.MessageTopic;

import java.time.LocalDateTime;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A {@link MessageHandler} that knows how to update various {@link TemperatureChart}s
 */
public class ChartUpdatingMessageHandler implements MessageHandler {

    private final RawHistoryTemperatureChart raw;
    private final HourlyHighLowTemperatureChart hiLow;

    public ChartUpdatingMessageHandler(
            RawHistoryTemperatureChart raw,
            HourlyHighLowTemperatureChart hiLow
    ) {
        this.raw = checkNotNull(raw);
        this.hiLow = checkNotNull(hiLow);
    }

    @Override
    public void handle(Message message) {
        LocalDateTime dateTime = LocalDateTime.now();
        Second now = Utils.secondFromTimestamp(dateTime);

        if (MessageTopic.TEMPCURR.equals(message.getMessageTopic())) {
            float temp = Float.parseFloat(message.getMessage());
            raw.addCurrentTempData(temp, now);
            hiLow.addTemp(temp, dateTime);
        }

        if (MessageTopic.TEMPMAX.equals(message.getMessageTopic())) {
            float temp = Float.parseFloat(message.getMessage());
            raw.addMaxTempData(temp, now);
        }

        if (MessageTopic.TEMPMIN.equals(message.getMessageTopic())) {
            float temp = Float.parseFloat(message.getMessage());
            raw.addMinTempData(temp, now);
        }
    }
}