package uk.ac.kent.jb695.co657.java.app.gui;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.statistics.DefaultStatisticalCategoryDataset;

import javax.swing.*;
import java.awt.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;

/**
 * A chart that displays the hourly high and low thresholds for all temperature readings reported to it
 */
public class HourlyHighLowTemperatureChart {

    private static final String HIGH = "High";
    private static final String LOW = "Low";
    private static final String TIMESTAMP_PATTERN = "dd/M k:mm";
    private static final String CHART_NAME = "Hourly High/Low";
    private static final String Y_KEY = "Hour";
    private static final String X_KEY = "Temperature";
    private static final int WIDTH = 550;
    private static final int HEIGHT = 300;

    private final DefaultStatisticalCategoryDataset data = new DefaultStatisticalCategoryDataset();
    private final JFrame window = new JFrame();

    public void render() {
        window.add(create7DayAveragesPanel());
        window.pack();
        window.setVisible(true);
    }

    private ChartPanel create7DayAveragesPanel() {
        ChartPanel chartPanel = new ChartPanel(create7DayAveragesChart());
        chartPanel.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        return chartPanel;
    }

    private JFreeChart create7DayAveragesChart() {
        return ChartFactory.createBarChart(
                CHART_NAME,
                Y_KEY,
                X_KEY,
                data,
                PlotOrientation.HORIZONTAL,
                true,
                false,
                false
        );
    }

    public void addTemp(float temp, LocalDateTime timestamp) {
        timestamp = timestamp.truncatedTo(ChronoField.HOUR_OF_DAY.getBaseUnit());
        String timestampKey = timestamp.format(DateTimeFormatter.ofPattern(TIMESTAMP_PATTERN));
        Number high = data.getMeanValue(HIGH, timestampKey);
        if (high == null || high.floatValue() < temp) {
            data.add(temp, 0, HIGH, timestampKey);
        }
        Number low = data.getMeanValue(LOW, timestampKey);
        if (low == null || low.floatValue() > temp) {
            data.add(temp, 0, LOW,  timestampKey);
        }
    }
}