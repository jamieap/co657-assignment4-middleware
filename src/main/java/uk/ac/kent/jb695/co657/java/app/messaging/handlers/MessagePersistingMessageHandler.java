package uk.ac.kent.jb695.co657.java.app.messaging.handlers;

import com.google.common.collect.ImmutableMap;
import com.google.common.primitives.Floats;
import uk.ac.kent.jb695.co657.java.app.messaging.messages.Message;
import uk.ac.kent.jb695.co657.java.app.messaging.messages.SensorReading;
import uk.ac.kent.jb695.co657.java.app.messaging.topics.MessageTopic;

import java.util.NavigableSet;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A {@link MessageHandler} that saves any {@link Message}s for which it has a database
 *      available for its {@link MessageTopic}
 */
public class MessagePersistingMessageHandler implements MessageHandler {

    private final ImmutableMap<MessageTopic, NavigableSet<SensorReading>> dbs;

    public MessagePersistingMessageHandler(
            NavigableSet<SensorReading> currTemp,
            NavigableSet<SensorReading> maxTemp,
            NavigableSet<SensorReading> minTemp
    ) {
        this.dbs = ImmutableMap.of(
                MessageTopic.TEMPCURR, checkNotNull(currTemp),
                MessageTopic.TEMPMAX, checkNotNull(maxTemp),
                MessageTopic.TEMPMIN, checkNotNull(minTemp)
        );
    }

    @Override
    public void handle(Message msg) {
        saveMessage(msg);
    }

    private void saveMessage(Message msg) {
        if (!dbs.containsKey(msg.getMessageTopic())) {
            return;
        }

        NavigableSet<SensorReading> db = dbs.get(msg.getMessageTopic());

        Float val = Floats.tryParse(msg.getMessage());
        if (val == null) {
            return;
        }

        db.add(SensorReading.valueOf(msg.getMessageTopic(), val));
    }
}