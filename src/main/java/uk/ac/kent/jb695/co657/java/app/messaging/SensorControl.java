package uk.ac.kent.jb695.co657.java.app.messaging;

import com.google.common.base.Throwables;
import com.google.common.util.concurrent.RateLimiter;
import jssc.SerialPort;
import jssc.SerialPortException;

/**
 * Provides a wrapper for issuing text-based commands down a {@link SerialPort} to a sensor device.
 * It also defines the 'protocol' for those commands.
 *
 * I was never able to get the K64F to reliable read command messages via serial in a low-power,
 * event driven way. Only a busy wait loop produced consistent results...
 */
public class SensorControl {

    private static final RateLimiter RATE_LIMITER = RateLimiter.create(1);

    private final SerialPort serialPort;

    public SensorControl(SerialPort serialPort) {
        this.serialPort = serialPort;
    }

    public void increaseMinTemp() {
        try {
            RATE_LIMITER.acquire();
            serialPort.writeString("LU");
        } catch (SerialPortException e) {
            Throwables.propagate(e);
        }
    }

    public void increaseMaxTemp() {
        try {
            RATE_LIMITER.acquire();
            serialPort.writeString("HU");
        } catch (SerialPortException e) {
            Throwables.propagate(e);
        }
    }

    public void decreaseMinTemp() {
        try {
            RATE_LIMITER.acquire();
            serialPort.writeString("LD");
        } catch (SerialPortException e) {
            Throwables.propagate(e);
        }
    }

    public void decreaseMaxTemp() {
        try {
            RATE_LIMITER.acquire();
            serialPort.writeString("HD");
        } catch (SerialPortException e) {
            Throwables.propagate(e);
        }
    }
}