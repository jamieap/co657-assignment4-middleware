package uk.ac.kent.jb695.co657.java.app.messaging.topics;


import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.fusesource.mqtt.client.Callback;
import org.fusesource.mqtt.client.Future;
import org.fusesource.mqtt.client.FutureConnection;
import org.fusesource.mqtt.client.QoS;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static com.google.common.base.Preconditions.checkNotNull;

/**
    A task that reports all topics used by the application to an MQTT topic.
    It does so on at a regular 1 minute interval through an
    internal {@link java.util.concurrent.ScheduledExecutorService}
 */
public class TopicReporter {

    private static final String TOPIC = "unikent/users/jb695/topics";
    private static final String NEWLINE = "\n";
    private static final int THREADS = 1;
    private static final Callback<Void> TOPIC_REPORTING_CALLBACK = new Callback<Void>() {
        @Override
        public void onSuccess(Void aVoid) {
            System.out.println("Successfully reported topics to " + TOPIC);
        }

        @Override
        public void onFailure(Throwable throwable) {
            System.out.println("Failed to report topics to " + TOPIC);
        }
    };

    private final FutureConnection mqttConnection;
    private final ScheduledThreadPoolExecutor scheduledExecutor = new ScheduledThreadPoolExecutor(
            THREADS,
            new ThreadFactoryBuilder().build()
    );

    public TopicReporter(FutureConnection mqttConnection) {
        this.mqttConnection = checkNotNull(mqttConnection);
    }

    public void start() {
        scheduledExecutor.scheduleAtFixedRate(createTopicPublishingRunnable(), 0, 60, TimeUnit.SECONDS);
    }

    private Runnable createTopicPublishingRunnable() {
        return () -> {
            StringBuilder stringBuilder = new StringBuilder();
            for (MessageTopic messageTopic : MessageTopic.values()) {
                stringBuilder.append(messageTopic.getMqttTopic()).append(NEWLINE);
            }
            Future<Void> resultFuture = mqttConnection.publish(
                    TOPIC,
                    stringBuilder.toString().getBytes(),
                    QoS.EXACTLY_ONCE,
                    true
            );
            resultFuture.then(TOPIC_REPORTING_CALLBACK);
        };
    }
}