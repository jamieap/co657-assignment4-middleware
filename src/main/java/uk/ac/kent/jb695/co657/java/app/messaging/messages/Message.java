package uk.ac.kent.jb695.co657.java.app.messaging.messages;

import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;
import uk.ac.kent.jb695.co657.java.app.messaging.topics.MessageTopic;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A data type pairing a {@link MessageTopic} with a {@link String} where the String is a
 * sensor reading value.
 */
public class Message {

    private final MessageTopic topic;
    private final String message;

    public Message(String message, MessageTopic topic) {
        checkArgument(!Strings.isNullOrEmpty(message));
        this.message = message;
        this.topic = checkNotNull(topic);
    }

    public MessageTopic getMessageTopic() {
        return topic;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("topic", topic.getMqttTopic())
                .add("message", message)
                .toString();
    }
}
