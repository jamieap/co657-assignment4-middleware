package uk.ac.kent.jb695.co657.java.app;

import org.jfree.data.time.Second;

import java.time.LocalDateTime;

public class Utils {

    public static Second secondFromTimestamp(LocalDateTime timestamp) {
        return new Second(
                timestamp.getSecond(),
                timestamp.getMinute(),
                timestamp.getHour(),
                timestamp.getDayOfMonth(),
                timestamp.getMonthValue(),
                timestamp.getYear()
        );
    }
}
