package uk.ac.kent.jb695.co657.java.app.persistence;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import org.jfree.data.time.Second;
import uk.ac.kent.jb695.co657.java.app.Utils;
import uk.ac.kent.jb695.co657.java.app.gui.RawHistoryTemperatureChart;
import uk.ac.kent.jb695.co657.java.app.gui.TemperatureChart;
import uk.ac.kent.jb695.co657.java.app.messaging.messages.SensorReading;
import uk.ac.kent.jb695.co657.java.app.messaging.topics.MessageTopic;
import uk.ac.kent.jb695.co657.java.app.gui.HourlyHighLowTemperatureChart;

import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.concurrent.Executors;

import static com.google.common.base.Preconditions.checkNotNull;

/**
    A task that repopulates the {@link TemperatureChart} with historic data,
    retrieved from a database via {@link DbUtils}
 */
public class MessageReplayer {

    private final Iterator<SensorReading> historicData;
    private final RawHistoryTemperatureChart raw;
    private final HourlyHighLowTemperatureChart hiLow;
    private final ListeningExecutorService executor = MoreExecutors.listeningDecorator(Executors.newSingleThreadExecutor());

    public MessageReplayer(
            Iterator<SensorReading> historicData,
            RawHistoryTemperatureChart raw,
            HourlyHighLowTemperatureChart hiLow
    ) {
        this.historicData = checkNotNull(historicData);
        this.raw = checkNotNull(raw);
        this.hiLow = checkNotNull(hiLow);
    }

    public ListenableFuture<Integer> reload() {
        return executor.submit(this::run);
    }

    private Integer run() {
        System.out.println("Historic data reloading started at: " + LocalDateTime.now());
        int count = reloadHistoryFromDb(historicData);
        System.out.println("Historic data reloading finished at: " + LocalDateTime.now());
        return count;
    }

    private int reloadHistoryFromDb(Iterator<SensorReading> db) {
        int count = 0;
        while (db.hasNext()) {
            count++;
            SensorReading data = db.next();
            Second second = Utils.secondFromTimestamp(data.getTimestamp());
            if (MessageTopic.TEMPMAX.equals(data.getTopic())) {
                raw.addMaxTempData(data.getValue(), second);
            }
            if (MessageTopic.TEMPMIN.equals(data.getTopic())) {
                raw.addMinTempData(data.getValue(), second);
            }
            if (MessageTopic.TEMPCURR.equals(data.getTopic())) {
                raw.addCurrentTempData(data.getValue(), second);
                hiLow.addTemp(data.getValue(), data.getTimestamp());
            }
        }
        return count;
    }
}