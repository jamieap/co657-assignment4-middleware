package uk.ac.kent.jb695.co657.java.app.messaging.handlers;

import uk.ac.kent.jb695.co657.java.app.messaging.messages.Message;

public interface MessageHandler {

    void handle(Message message);

}
