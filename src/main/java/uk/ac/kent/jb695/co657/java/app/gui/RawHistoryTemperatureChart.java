package uk.ac.kent.jb695.co657.java.app.gui;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

import javax.swing.*;
import java.awt.*;

/**
 * A chart that displays all raw temperature sensor reading data points recorded by this application.
 */
public class RawHistoryTemperatureChart {

    private static final String CHART_NAME = "Temperature";
    private static final String Y_KEY = "Time";
    private static final int WIDTH = 550;
    private static final int HEIGHT = 300;

    private final TimeSeries currentTemp = new TimeSeries("Current Temp", Second.class);
    private final TimeSeries minTemp = new TimeSeries("Min Limit", Second.class);
    private final TimeSeries maxTemp = new TimeSeries("Max Limit", Second.class);
    private final JFrame window = new JFrame();

    public void addMinTempData(Float point, Second second) {
        minTemp.addOrUpdate(second, point);
    }

    public void addMaxTempData(Float point, Second second) {
        maxTemp.addOrUpdate(second, point);
    }

    public void addCurrentTempData(Float point, Second second) {
        currentTemp.addOrUpdate(second, point);
    }

    public void render() {
        window.add(createRawHistoryPanel());
        window.pack();
        window.setVisible(true);
    }

    private ChartPanel createRawHistoryPanel() {
        ChartPanel chartPanel = new ChartPanel(createRawHistoryChart());
        chartPanel.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        return chartPanel;
    }

    private JFreeChart createRawHistoryChart() {
        TimeSeriesCollection collection = new TimeSeriesCollection();
        collection.addSeries(currentTemp);
        collection.addSeries(minTemp);
        collection.addSeries(maxTemp);
        return ChartFactory.createTimeSeriesChart(
                CHART_NAME,
                Y_KEY,
                CHART_NAME,
                collection,
                true,
                true,
                false
        );
    }
}
