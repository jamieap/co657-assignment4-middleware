package uk.ac.kent.jb695.co657.java.app;

import com.google.common.base.Optional;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import uk.ac.kent.jb695.co657.java.app.messaging.handlers.MessageHandler;
import uk.ac.kent.jb695.co657.java.app.messaging.messages.Message;
import uk.ac.kent.jb695.co657.java.app.messaging.messages.MessageParser;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * An {@link SerialPortEventListener} that reads String messages from a serial device when
 * an RXCHAR event is triggered.
 */
public class SerialPortReader implements SerialPortEventListener {

    private final SerialPort serialPort;
    private final MessageHandler handler;
    private final MessageParser msgParser;

    public SerialPortReader(SerialPort serialPort, MessageHandler handler, MessageParser msgParser) {
        this.serialPort = checkNotNull(serialPort);
        this.handler = checkNotNull(handler);
        this.msgParser = checkNotNull(msgParser);
    }

    /**
     * Reads continuously from an input stream provided by a {@link SerialPort},
     * until the newline byte is found, all bytes read up to that point are considered a single message.
     * The message bytes are handed to a {@link MessageParser} for validation, if it is valid the parsed
     * {@link Message} is handed to a {@link MessageHandler} for further processing.
     */
    public void serialEvent(SerialPortEvent serialPortEvent) {
        try {
            StringBuilder stringBuilder = new StringBuilder();
            String str = serialPort.readString(1);
            while (!"\n".equalsIgnoreCase(str)) {
                stringBuilder.append(str);
                str = serialPort.readString(1);
            }
            Optional<Message> maybeMessage = msgParser.parseMessage(stringBuilder.toString());
            if (maybeMessage.isPresent()) {
                handler.handle(maybeMessage.get());
            }
        } catch (SerialPortException e) {
            e.printStackTrace();
        }
    }
}