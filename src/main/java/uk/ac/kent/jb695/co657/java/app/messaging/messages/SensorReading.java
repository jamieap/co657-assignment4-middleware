package uk.ac.kent.jb695.co657.java.app.messaging.messages;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import uk.ac.kent.jb695.co657.java.app.messaging.topics.MessageTopic;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Comparator;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A serializable class for {@link Message}
 */
public class SensorReading implements Comparable<SensorReading>, Serializable {

    /*  This would be a lambda or at least an anonymous class, but that breaks {@link Serializable} */
    public static class SensorReadingComparator implements Comparator<SensorReading>, Serializable {

        public static SensorReadingComparator INSTANCE = new SensorReadingComparator();

        private SensorReadingComparator() {}

        @Override
        public int compare(SensorReading a, SensorReading b) {
            if (Objects.equal(a, b)) {
                return 0;
            }
            return a.getTimestamp().compareTo(b.getTimestamp());
        }
    }

    private final MessageTopic topic;
    private final LocalDateTime timestamp;
    private final Float value;

    public SensorReading(LocalDateTime timestamp, MessageTopic topic, Float value) {
        this.timestamp = checkNotNull(timestamp);
        this.topic = topic;
        this.value = value;
    }

    public MessageTopic getTopic() {
        return topic;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public Float getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SensorReading that = (SensorReading) o;
        return Objects.equal(getTimestamp(), that.getTimestamp());
    }

    @Override
    public int hashCode() {
        return getTimestamp().hashCode();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("topic", topic)
                .add("timestamp", timestamp)
                .add("value", value)
                .toString();
    }

    @Override
    public int compareTo(SensorReading that) {
        return comparator().compare(this, that);
    }

    public static Comparator<SensorReading> comparator() {
        return SensorReadingComparator.INSTANCE;
    }

    public static SensorReading valueOf(MessageTopic topic, Float val) {
        return new SensorReading(LocalDateTime.now(), topic, val);
    }
}