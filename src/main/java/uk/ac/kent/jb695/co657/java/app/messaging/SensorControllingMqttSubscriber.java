package uk.ac.kent.jb695.co657.java.app.messaging;

import com.google.common.base.Optional;
import com.google.common.base.Throwables;
import org.fusesource.mqtt.client.Callback;
import org.fusesource.mqtt.client.FutureConnection;
import org.fusesource.mqtt.client.Message;
import uk.ac.kent.jb695.co657.java.app.messaging.topics.MessageTopic;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * An MQTT message listener for sensor control messages published to an MQTT server.
 * It translates MQTT messages into method calls on {@link SensorControl} for remote control
 * of a sensor device
 */
public class SensorControllingMqttSubscriber {

    private static final String UP = "UP";
    private static final String DOWN = "DOWN";
    private static final Callback<byte[]> SUBSCRIBER_CALLBACK = new Callback<byte[]>() {
        @Override
        public void onSuccess(byte[] bytes) {
            System.out.println("Successfully subscribed to topics");
        }

        @Override
        public void onFailure(Throwable e) {
            System.out.println("Failed to subscribe to control topics " + e.toString());
        }
    };

    private final FutureConnection mqttConnection;
    private final ExecutorService executor;
    private final SensorControl sensorControl;

    public SensorControllingMqttSubscriber(FutureConnection mqttConnection, SensorControl sensorControl) {
        this.mqttConnection = checkNotNull(mqttConnection);
        this.sensorControl = checkNotNull(sensorControl);
        this.executor = Executors.newSingleThreadExecutor();
    }

    public void start() throws Exception {
        mqttConnection.subscribe(MessageTopic.getControlTopics()).then(SUBSCRIBER_CALLBACK);
        System.out.println("Attempting to subscribe to control topics");
        executor.submit(createMessageListeningRunnable());
    }

    private Runnable createMessageListeningRunnable() {
        return () -> {
            while (true) {
                try {
                    processMessage();
                } catch (Exception e) {
                    Throwables.propagate(e);
                }
            }
        };
    }

    private void processMessage() throws Exception {
        Message msg = mqttConnection.receive().await();
        Optional<MessageTopic> maybeTopic = MessageTopic.valueOfMqttTopic(msg.getTopic());
        checkArgument(maybeTopic.isPresent(), "Could not identify topic for " + msg.getTopic());
        MessageTopic topic = maybeTopic.get();

        String msgPayload = new String(msg.getPayload());

        if (MessageTopic.TEMPMAXCTRL.equals(topic)) {
            if (UP.equalsIgnoreCase(msgPayload)) {
                sensorControl.increaseMaxTemp();
            }
            if (DOWN.equalsIgnoreCase(msgPayload)) {
                sensorControl.decreaseMaxTemp();
            }
        }
        if (MessageTopic.TEMPMINCTRL.equals(topic)) {
            if (UP.equalsIgnoreCase(msgPayload)) {
                sensorControl.increaseMinTemp();
            }
            if (DOWN.equalsIgnoreCase(msgPayload)) {
                sensorControl.decreaseMinTemp();
            }
        }
    }
}