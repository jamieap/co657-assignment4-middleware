package uk.ac.kent.jb695.co657.java.app;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterators;
import jssc.SerialPort;
import jssc.SerialPortException;
import org.fusesource.mqtt.client.FutureConnection;
import org.fusesource.mqtt.client.MQTT;
import org.mapdb.DB;
import uk.ac.kent.jb695.co657.java.app.gui.RawHistoryTemperatureChart;
import uk.ac.kent.jb695.co657.java.app.gui.TemperatureChart;
import uk.ac.kent.jb695.co657.java.app.messaging.SensorControl;
import uk.ac.kent.jb695.co657.java.app.messaging.SensorControllingMqttSubscriber;
import uk.ac.kent.jb695.co657.java.app.messaging.handlers.MessagePersistingMessageHandler;
import uk.ac.kent.jb695.co657.java.app.messaging.handlers.MqttProducingMessageHandler;
import uk.ac.kent.jb695.co657.java.app.messaging.messages.MessageParser;
import uk.ac.kent.jb695.co657.java.app.messaging.messages.SensorReading;
import uk.ac.kent.jb695.co657.java.app.messaging.topics.TopicReporter;
import uk.ac.kent.jb695.co657.java.app.persistence.DbUtils;
import uk.ac.kent.jb695.co657.java.app.persistence.MessageReplayer;
import uk.ac.kent.jb695.co657.java.app.gui.HourlyHighLowTemperatureChart;
import uk.ac.kent.jb695.co657.java.app.messaging.handlers.ChartUpdatingMessageHandler;
import uk.ac.kent.jb695.co657.java.app.messaging.handlers.MultiplexingMessageHandler;

import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.NavigableSet;

/*
    Application entry class
 */
public class SensorMiddleware {

    private static final int MQTT_PORT = 1883;
    private static final String MQTT_HOST = "localhost";
    private static final String MQTT_CLIENT_ID = "jb695";

    public static void main(String[] args) throws Exception {
        if (args.length == 0 || Strings.isNullOrEmpty(args[0])) {
            throw new RuntimeException("Provide the path to your serial port as an arg e.g. /dev/tty.usbmodem1452 or COM3");
        }
        new SensorMiddleware().run(args[0]);
    }

    /* This method wires up the application. Its becoming unruly enough that dependency injection
        would be a reasonable idea, but that seems overkill for this assignment.
     */
    private void run(String port) throws Exception {
        MQTT mqtt = createMqttClient(MQTT_HOST, MQTT_PORT, MQTT_CLIENT_ID);

        FutureConnection asyncConnection = mqtt.futureConnection();
        asyncConnection.connect();

        HourlyHighLowTemperatureChart hiLowChart = new HourlyHighLowTemperatureChart();
        RawHistoryTemperatureChart rawChart = new RawHistoryTemperatureChart();
        TemperatureChart chart = new TemperatureChart(rawChart, hiLowChart);

        DB db = DbUtils.initDb();
        NavigableSet<SensorReading> currentTempData = DbUtils.createOrGetDb(db, DbUtils.CURRENT_TEMP_DB);
        NavigableSet<SensorReading> maxTempData = DbUtils.createOrGetDb(db, DbUtils.TEMP_MAX_DB);
        NavigableSet<SensorReading> minTempData = DbUtils.createOrGetDb(db, DbUtils.TEMP_MIN_DB);

        Iterator<SensorReading> dbIterator = Iterators.concat(
                currentTempData.iterator(),
                maxTempData.iterator(),
                minTempData.iterator()
        );
        MessageReplayer replayer = new MessageReplayer(dbIterator, rawChart, hiLowChart);
        System.out.println("Reloaded " + replayer.reload().get() + " recorded data points");

        MqttProducingMessageHandler mqttProducerHandler = new MqttProducingMessageHandler(asyncConnection);
        ChartUpdatingMessageHandler tempProviderHandler = new ChartUpdatingMessageHandler(rawChart, hiLowChart);
        MessagePersistingMessageHandler msgDb = new MessagePersistingMessageHandler(currentTempData, minTempData, maxTempData);

        MultiplexingMessageHandler messageHandler = new MultiplexingMessageHandler(
                ImmutableSet.of(
                        msgDb,
                        mqttProducerHandler,
                        tempProviderHandler
                )
        );

        SerialPort serialPort = createSerialPort(port);
        if (serialPort != null) {
            SerialPortReader serialPortReader = new SerialPortReader(
                    serialPort,
                    messageHandler,
                    new MessageParser()
            );
            serialPort.addEventListener(serialPortReader, SerialPort.MASK_RXCHAR);
        }

        TopicReporter topicReporter = new TopicReporter(asyncConnection);
        topicReporter.start();

        SensorControl sensorControl = new SensorControl(serialPort);

        SensorControllingMqttSubscriber controlSubscriber = new SensorControllingMqttSubscriber(
                asyncConnection,
                sensorControl
        );

        controlSubscriber.start();
        chart.renderGui();

        while (true) {
            Thread.sleep(Long.MAX_VALUE);
        }
    }

    private MQTT createMqttClient(String host, Integer port, String clientId) throws URISyntaxException {
        MQTT mqtt = new MQTT();
        mqtt.setHost(host, port);
        mqtt.setClientId(clientId);
        mqtt.setConnectAttemptsMax(3);
        mqtt.setReconnectAttemptsMax(9999);
        mqtt.setReconnectBackOffMultiplier(2);
        mqtt.setCleanSession(true);
        mqtt.setMaxWriteRate(1024);
        return mqtt;
    }

    private static SerialPort createSerialPort(String portPath) {
        try {
            SerialPort serial = new SerialPort(portPath);
            serial.openPort();
            serial.setParams(SerialPort.BAUDRATE_38400, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
            serial.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN | SerialPort.FLOWCONTROL_RTSCTS_OUT);
            return serial;
        } catch (SerialPortException e) {
            System.out.println("Failed to open serial port: " + e.toString());
            return null;
        }
    }
}