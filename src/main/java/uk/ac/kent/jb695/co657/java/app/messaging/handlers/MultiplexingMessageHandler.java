package uk.ac.kent.jb695.co657.java.app.messaging.handlers;

import com.google.common.collect.ImmutableSet;
import uk.ac.kent.jb695.co657.java.app.messaging.messages.Message;

/**
 * A {@link MessageHandler} that echos any {@link Message}s it receives to 0 or more {@link MessageHandler}s
 */
public class MultiplexingMessageHandler implements MessageHandler {

    private final ImmutableSet<MessageHandler> delegates;

    public MultiplexingMessageHandler(Iterable<MessageHandler> delegates) {
        this.delegates = ImmutableSet.copyOf(delegates);
    }

    @Override
    public void handle(Message message) {
        delegates.forEach(messageHandler -> messageHandler.handle(message));
    }
}
