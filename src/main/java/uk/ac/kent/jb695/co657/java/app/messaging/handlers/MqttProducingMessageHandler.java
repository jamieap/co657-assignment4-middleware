package uk.ac.kent.jb695.co657.java.app.messaging.handlers;

import org.fusesource.mqtt.client.Callback;
import org.fusesource.mqtt.client.Future;
import org.fusesource.mqtt.client.FutureConnection;
import org.fusesource.mqtt.client.QoS;
import uk.ac.kent.jb695.co657.java.app.messaging.messages.Message;
import uk.ac.kent.jb695.co657.java.app.messaging.topics.MessageTopic;

import java.time.ZonedDateTime;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A {@link MessageHandler} that reports any {@link Message}s it receives to the associated MQTT topic
 * via {@link MessageTopic#getMqttTopic()}
 */
public class MqttProducingMessageHandler implements MessageHandler {

    private static final QoS MESSAGE_QOS = QoS.EXACTLY_ONCE;
    private static final Callback<Void> PRODUCER_RESULT_CALLBACK = new Callback<Void>() {
        public void onSuccess(Void aVoid) {
            System.out.println(ZonedDateTime.now().toLocalDateTime().toString() + " Successfully produced message to MQTT server");
        }

        public void onFailure(Throwable throwable) {
            System.out.println("Failed to produce MQTT message: " + throwable.toString());
        }
    };

    private final FutureConnection mqttConnection;

    public MqttProducingMessageHandler(FutureConnection mqttConnection) {
        this.mqttConnection = checkNotNull(mqttConnection);
    }

    @Override
    public void handle(Message message) {
        if (!mqttConnection.isConnected()) {
            return;
        }
        Future<Void> asyncResult = mqttConnection.publish(
                message.getMessageTopic().getMqttTopic(),
                message.getMessage().getBytes(),
                MESSAGE_QOS,
                true
        );
        asyncResult.then(PRODUCER_RESULT_CALLBACK);
    }
}