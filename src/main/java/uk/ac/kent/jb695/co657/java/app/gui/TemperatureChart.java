package uk.ac.kent.jb695.co657.java.app.gui;

import org.jfree.ui.ApplicationFrame;

import static com.google.common.base.Preconditions.checkNotNull;

public class TemperatureChart extends ApplicationFrame {

    private final HourlyHighLowTemperatureChart highLowChart;
    private final RawHistoryTemperatureChart rawChart;

    public TemperatureChart(
            RawHistoryTemperatureChart rawChart,
            HourlyHighLowTemperatureChart highLowChart
    ) {
        super("Temperature");
        this.highLowChart = checkNotNull(highLowChart);
        this.rawChart = checkNotNull(rawChart);
    }

    public void renderGui() {
        highLowChart.render();
        rawChart.render();
    }
}