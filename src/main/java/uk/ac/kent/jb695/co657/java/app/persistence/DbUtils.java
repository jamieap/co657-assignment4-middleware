package uk.ac.kent.jb695.co657.java.app.persistence;

import org.mapdb.DB;
import org.mapdb.DBMaker;
import uk.ac.kent.jb695.co657.java.app.messaging.messages.SensorReading;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.NavigableSet;

public class DbUtils {

    private static final String HOME_DIR = System.getProperty("user.home");
    private static final String DB_FILE_NAME = "sensor-data";

    public static final String CURRENT_TEMP_DB = "curr-temp";
    public static final String TEMP_MIN_DB = "temp-min";
    public static final String TEMP_MAX_DB = "temp-max";

    /**
        Retrieves (or creates) a file-persisted {@link java.util.TreeSet} of {@link SensorReading}
     */
    public static NavigableSet<SensorReading> createOrGetDb(DB db, String dbName) throws IOException {
        return db.exists(dbName) ?
                db.getTreeSet(dbName) :
                db.createTreeSet(dbName)
                        .comparator(SensorReading.comparator())
                        .nodeSize(10)
                        .make();
    }

    /**
        This creates a memory mapped database that is persisted in a file in the user's home directory
        The API to this database is largely through objects that implement {@link Collection}
        The file used to store data is allocated in 1MB blocks
     */
    public static DB initDb() throws IOException {
        Path dbPath = Paths.get(HOME_DIR, DB_FILE_NAME);
        File dbFile = Files.exists(dbPath) ? dbPath.toFile() : Files.createFile(dbPath).toFile();

        return DBMaker.newFileDB(dbFile)
                .mmapFileEnableIfSupported()
                .closeOnJvmShutdown()
                .cacheHardRefEnable()
                .transactionDisable()
                .make();
    }
}
