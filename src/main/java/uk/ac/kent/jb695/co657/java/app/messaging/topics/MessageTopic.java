package uk.ac.kent.jb695.co657.java.app.messaging.topics;

import com.google.common.base.Optional;
import org.fusesource.mqtt.client.QoS;
import org.fusesource.mqtt.client.Topic;

import java.io.Serializable;

/**
    An enumeration of message types in this application.
    Both the word message and this enum itself are conflated in different contexts throughout the code.
    The important bit is it provides typed MQTT topics (as apposed to Strings) and can be used
        to tag the various data reported by the K64F.
 */
public enum MessageTopic implements Serializable {

    TEMPMAX {
        @Override
        public String getMqttTopic() {
            return "canterbury/beverley-road/temperature/max";
        }
    },
    TEMPMIN {
        @Override
        public String getMqttTopic() {
            return "canterbury/beverley-road/temperature/min";
        }
    },
    TEMPCURR {
        @Override
        public String getMqttTopic() {
            return "canterbury/beverley-road/temperature/current";
        }
    },
    TEMPMAXCTRL {
        @Override
        public String getMqttTopic() {
            return "canterbury/beverley-road/control/temperature/max";
        }
    },
    TEMPMINCTRL {
        @Override
        public String getMqttTopic() {
            return "canterbury/beverley-road/control/temperature/min";
        }
    };

    public abstract String getMqttTopic();

    public static Topic[] getControlTopics() {
        Topic[] topics = new Topic[2];
        topics[0] = new Topic(TEMPMAXCTRL.getMqttTopic(), QoS.EXACTLY_ONCE);
        topics[1] = new Topic(TEMPMINCTRL.getMqttTopic(), QoS.EXACTLY_ONCE);
        return topics;
    }

    public static Optional<MessageTopic> valueOfMqttTopic(String mqttTopic) {
        for (MessageTopic topic : MessageTopic.values()) {
            if (mqttTopic.equalsIgnoreCase(topic.getMqttTopic())) {
                return Optional.of(topic);
            }
        }
        return Optional.absent();
    }
}
